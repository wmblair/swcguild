function CheckName(){   
    var name = document.getElementById('nameField').value;
    if(name.trim() == ""){
        alert("Name is a required field");
    return false;
    }
    return true;
}

function CheckEmailorPhone(){
    var email = document.getElementById('emailField').value;
    var phone = document.getElementById('phoneField').value;
    
    if(email.trim() == "" && phone.trim() == ""){
        alert("Email or Phone is a required field");
    return false;   
    }
    return true;
}

function CheckReason(){        
    var inquiryReason = document.getElementById('reasonSelection').value;
    var additionalText = document.getElementById('additionalInfo').value;    
    
    if(inquiryReason == "Op4" && additionalText.trim() == ""){      //Op4 is 'other' option value
        alert("Additional information is required with 'Other' selection");
        return false;
    }
    return true;
}

function CheckDaysToContact(){
    var daysToContact = document.getElementsByClassName('daysOfWeek');
    var requiredList = document.getElementsByClassName('checkRequired');
    var counter = 0;

    for(var i = 0; i < daysToContact.length; i++){           
        if(!daysToContact[i].checked)
        counter++;
    }
    if(counter == daysToContact.length){
        alert("At least one day of contact is required");
        return false;
    }
return true;    
}

function CheckRequiredFields(){
    
if(CheckName() && CheckEmailorPhone() && CheckReason() && CheckDaysToContact())
    return true;
return false;
}
