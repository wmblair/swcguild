function playLuckySevens(bet) {
   
    var startingBet = bet, highestAmtWon = 0, rollsWhenBroke = 0, rollsAtHighest = 0;                         
        console.log("--------------- GAME START ---------------"); //added console.log commands for the user to see the game being played in the console
      
        while(bet > 0) {

        var a = Math.floor(Math.random()*6+1);
        var b = Math.floor(Math.random()*6+1);           
                        
            console.log("amount currently held: $" + bet); 
            
            console.log("------ Roll Number: (" + (rollsWhenBroke+1) + ") ------");
            console.log((a+b) + " (" + a + "+" + b + ")");
            
            if((a+b) == 7) {
                bet = (bet+4);
                console.log("You won $4!!!!");
                
                if(bet > highestAmtWon){ //
                    highestAmtWon = bet;
                    rollsAtHighest = (rollsWhenBroke+1);
                    console.log("*** rolls until highest so far: " + rollsAtHighest + " ***");
                }
            }
            else
            bet--;   
           
            rollsWhenBroke++;           
        }
      
        console.log("number of rolls when broke: " + rollsWhenBroke);
        console.log("number of rolls at highest amount: " + rollsAtHighest); 
        console.log("highest amount won: $" + highestAmtWon);
        console.log("---------------- GAME OVER ----------------");

        hideResults(false);
    
        document.getElementById('playButton').textContent = "Play Again";
    
        document.getElementById('startingBetTD').innerHTML = "$" + startingBet;
        document.getElementById('rollsWhenBrokeTD').innerHTML = rollsWhenBroke;
        document.getElementById('highestAmtWonTD').innerHTML = "$" + highestAmtWon;
        document.getElementById('rollsAtHighestTD').innerHTML = rollsAtHighest;
  }

function hideResults(toggle){
    if(toggle)
        document.getElementById('resultsDiv').style.display = "none";
    else
        document.getElementById('resultsDiv').style.display = "block";    
}

//  var a = Math.floor(Math.random()*11+2) //the alternative way to generate a number between 2-12